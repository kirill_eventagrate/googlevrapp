﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;

public class UIInterative : MonoBehaviour
{
    public SteamVR_LaserPointer steamVR_LaserPointer;

    // Start is called before the first frame update
    void Start()
    {
        //PointerClick += SteamVR_LaserPointer_PointerClick;
        steamVR_LaserPointer.PointerClick += SteamVR_LaserPointer_PointerClick;
        steamVR_LaserPointer.PointerIn += SteamVR_LaserPointer_PointerIn;
        steamVR_LaserPointer.PointerOut += SteamVR_LaserPointer_PointerOut;
    }

    private void SteamVR_LaserPointer_PointerOut(object sender, PointerEventArgs e)
    {
        if (steamVR_LaserPointer.previousContact.GetComponent<GoogleSearchProdScript>())
        {
            steamVR_LaserPointer.previousContact.GetComponent<GoogleSearchProdScript>().btn.interactable = true;
        }
    }

    private void SteamVR_LaserPointer_PointerIn(object sender, PointerEventArgs e)
    {
        if (steamVR_LaserPointer.previousContact.GetComponent<GoogleSearchProdScript>())
        {
            steamVR_LaserPointer.previousContact.GetComponent<GoogleSearchProdScript>().btn.interactable = false;
        }
    }

    private void SteamVR_LaserPointer_PointerClick(object sender, PointerEventArgs e)
    {
        if (steamVR_LaserPointer.previousContact.gameObject.activeSelf)
        {
            if (steamVR_LaserPointer.previousContact.GetComponent<MainMenuButton>())
            {
                steamVR_LaserPointer.previousContact.GetComponent<MainMenuButton>().toDoEvent.Invoke();
                return;
            }

            if (steamVR_LaserPointer.previousContact.GetComponent<SwapOption>())
            {
                steamVR_LaserPointer.previousContact.GetComponent<SwapOption>().SpawnSwap(gameObject);
                return;
            }
            if (steamVR_LaserPointer.previousContact.GetComponent<PhoneScript>())
            {
                gameObject.GetComponent<ControllerAction>().GrabObject(steamVR_LaserPointer.previousContact.gameObject);
                return;
            }

        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
