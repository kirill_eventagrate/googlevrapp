﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapTopScript : MonoBehaviour
{
    static public LapTopScript instance = null;

    public GameObject HTCViveComponent;
    public GameObject StoreTarget;
    public GameObject StartSearchPage;
    public GameObject GoogleSearchImage;
    public GameObject[] SearchPages;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToStore()
    {
        PlayerScript.instance.GoToTargetPos(StoreTarget);
        /*Loom.QueueOnMainThread(() => {
            PlayerScript.instance.Home.SetActive(false);
            PlayerScript.instance.Store.SetActive(true);
            HTCViveComponent.transform.position = StoreTarget.transform.position;
            HTCViveComponent.transform.rotation = StoreTarget.transform.rotation;
            PlayerScript.instance.ShowScreen();
        }, 0.5f);*/
    }

    public void ShowGooglePreviewProd(bool _show)
    {
        GoogleSearchImage.SetActive(!GoogleSearchImage.activeSelf);
        //print("ShowGooglePreviewProd");
    }

    public void ShowSearchPage(int _pageIndex)
    {
        StartSearchPage.SetActive(false);
        ShowGooglePreviewProd(false);
        SearchPages[0].SetActive(false);
        SearchPages[1].SetActive(false);
        SearchPages[_pageIndex].SetActive(true);
    }

    public void ResetState()
    {
        SearchPages[0].SetActive(false);
        SearchPages[1].SetActive(false);
        StartSearchPage.SetActive(true);
        ShowGooglePreviewProd(false);
    }
}
