﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour
{
    float angleY;
    Quaternion originRotation;
    public float Sence;
    public int Direction;
    public float Y;

    // Start is called before the first frame update
    void Start()
    {
        originRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Direction == 0)
        {
            transform.Rotate(0, 0, -Sence * Time.deltaTime, Space.Self);
        } else if (Direction == 1) {
            transform.Rotate(0, 0, Sence * Time.deltaTime, Space.Self);
        }
        /*Quaternion rotationY = Quaternion.AngleAxis(angleY * Sence, Vector3.right);
        transform.rotation = originRotation * rotationY;*/
        
    }

    public void ShowRing()
    {
        iTween.MoveTo(gameObject, iTween.Hash("y", Y, "time", 1f, "islocal", true, "delay", 0));
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", 1.0f, "delay", 0));
    }

    public void HideRing()
    {
        iTween.MoveTo(gameObject, iTween.Hash("y", 0, "time", 1f, "islocal", true, "delay", 0));
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", 1.0f, "delay", 0));
    }
}
