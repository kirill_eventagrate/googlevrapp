﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreProduct : MonoBehaviour
{
    public GameObject ParentObj;
    public GameObject ProductMoveTarget;
    public GameObject ProductUI;
    public GameObject CheckOutCounterTarget;
    public InterectionObj interection;
    public GameObject ProductModelParent;
    public Animator animator;
    //public Animator animatorBigTitle;
    public Animator animatorNamshi;
    public Animator animatorDescription;
    public Button[] ColorsBtn;
    public Button[] SwapBtns;
    public int ChoosProductIndex = 0;

    Vector3 StartPos;
    Vector3 startRot;
    Quaternion UIStartRot;

    int ModeIndex = 0;
    bool isShowNamshiPage = false;
    bool CanUse = true;
    // Start is called before the first frame update
    void Start()
    {
        StartPos = transform.position;
        startRot = transform.eulerAngles;
        UIStartRot = ProductUI.transform.localRotation;
        HideUI();
        HideUINamshi();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        //ProductUI.transform.LookAt(FocusCamera.transform.position);
    }

    public void DoAction()
    {
        if (PlayerScript.instance.ChoosFocusProduct && PlayerScript.instance.ChoosFocusProduct != this)
        {
            return;
        }
        switch (ModeIndex)
        {
            case 0:
                GoToTargetPos();
                break;
            case 1:
                GoToStartPos();
                break;
        }
    }

    public void UnSelectBtns()
    {
        foreach (Button _bnt in ColorsBtn)
        {
            _bnt.interactable = true;
        }
    }

    public void UnSelectSwapBtns()
    {
        foreach (Button _bnt in SwapBtns)
        {
            _bnt.interactable = true;
        }
    }

    void GoToTargetPos()
    {
        if (CanUse)
        {
            CanUse = false;
            transform.SetParent(ProductMoveTarget.transform);
            ProductModelParent.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            PlayerScript.instance.ChoosFocusProduct = this;
            ProductUI.transform.localRotation = Quaternion.identity;
            PlayerScript.instance.interectionObj = interection;
            ProductModelParent.transform.eulerAngles = new Vector3(0, 0, 20);
            iTween.MoveTo(gameObject, iTween.Hash("position", Vector3.zero, "islocal", true, "time", 2.0f, "delay", 0));
            iTween.RotateTo(gameObject, iTween.Hash("rotation", Vector3.zero, "islocal", true, "time", 1.5f, "delay", 0));
            ModeIndex = 1;
            ShowUI();
            ShowUINamshi();
            
            Loom.QueueOnMainThread(() => {
                CanUse = true;
            }, 1.75f);
        }
    }

    public void GoToStartPos()
    {
        if (CanUse)
        {
            CanUse = false;
            transform.SetParent(ParentObj.transform);
            ProductModelParent.transform.localScale = Vector3.one;
            PlayerScript.instance.ChoosFocusProduct = null;
            ProductUI.transform.localRotation = UIStartRot;
            PlayerScript.instance.interectionObj = null;
            /*interection.gameObject.transform.eulerAngles = Vector3.zero;
            ProductModelParent.transform.eulerAngles = Vector3.zero;*/
            interection.ResetRot();
            ProductModelParent.transform.localRotation = Quaternion.identity;

            iTween.MoveTo(gameObject, iTween.Hash("position", StartPos, "time", 2.0f, "delay", 0));
            iTween.RotateTo(gameObject, iTween.Hash("rotation", startRot, "time", 1.5f, "delay", 0.1f));
            ModeIndex = 0;
            HideUI();
            HideUINamshi();
            if (isShowNamshiPage)
            {
                HideUINamshi();
                isShowNamshiPage = false;
            }
            Loom.QueueOnMainThread(() => {
                CanUse = true;
            }, 1.75f);
        }
    }

    public void ClickOnCheckOut()
    {
        GoToStartPos();
        PlayerScript.instance.GoToTargetPos(CheckOutCounterTarget);
        CheckOutCounter.instance.ChooseProduct(ChoosProductIndex);
        Loom.QueueOnMainThread(() => {
            CheckOutCounter.instance.StartAnimation();
        }, 0.5f);
    }



    void ShowUI()
    {
        animator.SetFloat("Direction", 1);
        animator.Play("ShowHideProductUINamshi", -1, 0);
    }

    void HideUI()
    {
        animator.SetFloat("Direction", -1);
        animator.Play("ShowHideProductUINamshi", -1, 1);
    }


    void ShowUINamshi()
    {
        animatorNamshi.SetFloat("Direction", -1);
        animatorNamshi.Play("ShowHideProductUINamshi", -1, 1);

        animatorDescription.SetFloat("Direction", -1);
        animatorDescription.Play("ShowHideProductUINamshi", -1, 1);
    }

    void HideUINamshi()
    {
        animatorNamshi.SetFloat("Direction", 1);
        animatorNamshi.Play("ShowHideProductUINamshi", -1, 0);

        animatorDescription.SetFloat("Direction", 1);
        animatorDescription.Play("ShowHideProductUINamshi", -1, 0);
    }

    public void ShowNamshiPage()
    {
        if (CanUse)
        {
            if (!isShowNamshiPage)
            {
                ShowUINamshi();
                isShowNamshiPage = true;
            }
            else
            {
                HideUINamshi();
                isShowNamshiPage = false;
            }
        }
    }
}