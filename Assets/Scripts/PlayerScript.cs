﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    static public PlayerScript instance = null;
    public Animator animator;
    public Vector3 StartPos;

    public StoreProduct ChoosFocusProduct = null;
    public bool ChoosGoogle = false;

    public GameObject Home;
    public GameObject Store;

    public InterectionObj interectionObj = null;
    

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        StartPos = transform.position;
        ShowScreen();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HideScreen()
    {
        animator.SetFloat("Direction", -1);
        animator.Play("ShowHideScreen", -1, 1);
    }

    public void ShowScreen()
    {
        animator.SetFloat("Direction", 1);
        animator.Play("ShowHideScreen", -1, 0);
    }

    public void GoToStartPos()
    {
        HideScreen();
        Loom.QueueOnMainThread(() => {
            Store.SetActive(false);
            Home.SetActive(true);
            transform.position = StartPos;
            ShowScreen();
        }, 0.5f);
    }

    public void GoToTargetPos(GameObject _target)
    {
        HideScreen();
        Loom.QueueOnMainThread(() => {
            Home.SetActive(false);
            Store.SetActive(true);
            transform.position = _target.transform.position;
            transform.rotation = _target.transform.rotation;
            ShowScreen();
        }, 0.5f);
    }

    public void GoToCheckOutCounterPos()
    {

    }
}
