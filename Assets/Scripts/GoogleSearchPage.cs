﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleSearchPage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        LapTopScript.instance.GoToStore();
        ResetSearchState();
    }

    public void ResetSearchState()
    {
        LapTopScript.instance.ResetState();
    }
}
