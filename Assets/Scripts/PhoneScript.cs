﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PhoneScript : MonoBehaviour
{
    public GameObject[] ModeScreens;
    public Button[] ProductsVideoBtn;
    public VideoPlayer[] VideoPlayers;
    public InterectionObj[] Products;
    public GameObject Hologram;
    public ControllerAction controllerAction;
    public Ring[] rings;
    int VideoBtnIndex = 0;
    int ModeIndex = 0;
    

    Vector3 startPos;
    Vector3 startRot;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        startRot = transform.eulerAngles;
        SetSelectVideo(VideoBtnIndex);
        //HideRings();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetSelectMode(int _modeIndex)
    {
        ModeIndex = _modeIndex;
        foreach (GameObject Mode in ModeScreens)
        {
            Mode.SetActive(false);
        }
        ModeScreens[ModeIndex].SetActive(true);
    }

    void SetSelectVideo(int _index)
    {
        VideoBtnIndex = _index;
        ProductsVideoBtn[VideoBtnIndex].Select();
    }

    void PlaySelectVideo(int _index)
    {
        VideoPlayers[VideoBtnIndex].Stop();
        foreach (VideoPlayer videoPlayer in VideoPlayers)
        {
            videoPlayer.gameObject.SetActive(false);
        }
        VideoBtnIndex = _index;
        VideoPlayers[VideoBtnIndex].gameObject.SetActive(true);
        VideoPlayers[VideoBtnIndex].time = 0;
        VideoPlayers[VideoBtnIndex].Prepare();
        VideoPlayers[VideoBtnIndex].Play();
    }

    void PlayNextVideo()
    {
        if ((VideoBtnIndex + 1) < ProductsVideoBtn.Length)
        {
            PlaySelectVideo(VideoBtnIndex + 1);
        }
    }

    void PlayPrevVideo()
    {
        if ((VideoBtnIndex - 1) >= 0)
        {
            PlaySelectVideo(VideoBtnIndex - 1);
        }
    }

    void SetSelectNextVideoBtn()
    {
        if ((VideoBtnIndex + 1) < ProductsVideoBtn.Length)
        {
            SetSelectVideo(VideoBtnIndex + 1);
        }
    }

    void SetSelectPrevVideoBtn()
    {
        if ((VideoBtnIndex - 1) >= 0)
        {
            SetSelectVideo(VideoBtnIndex - 1);
        }
    }

    public void SetPrevMode()
    {
        if ((ModeIndex - 1) >= 0)
        {
            SetSelectMode(ModeIndex - 1);
            SetSelectVideo(VideoBtnIndex);
        }
    }

    

    public void DoLeftAction()
    {
        /*switch (ModeIndex)
        {
            case 1:
                PlayPrevVideo();
                break;
        }*/
    }

    public void DoRightAction()
    {
        /*switch (ModeIndex)
        {
            case 1:
                PlayNextVideo();
                break;
        }*/
    }

    public void DoUpAction()
    {
        switch (ModeIndex)
        {
            case 0:
                //SetSelectPrevVideoBtn();
                break;
            case 1:
                VideoPlayers[VideoBtnIndex].Stop();
                HideRings();
                SetSelectMode(0);
                break;
            case 2:
                SetSelectMode(1);
                ShowRings();
                Products[VideoBtnIndex].HideObj();
                PlaySelectVideo(VideoBtnIndex);
                break;
            /*case 3:
                SetSelectMode(2);
                Products[VideoBtnIndex].HideObj();
                break;*/
        }
    }

    public void DoDownAction()
    {
        switch (ModeIndex)
        {
            case 0:
                SetSelectNextVideoBtn();
                break;
        }
    }

    public void DoAction()
    {
        switch (ModeIndex)
        {
            case 0:
                SetSelectMode(1);
                ShowRings();
                PlaySelectVideo(VideoBtnIndex);
                break;
            case 1:
                VideoPlayers[VideoBtnIndex].Stop();
                HideRings();
                Products[VideoBtnIndex].ShowObj();
                SetSelectMode(2);
                break;
            case 2:
                Products[VideoBtnIndex].HideObj();
                SetSelectMode(3);
                Loom.QueueOnMainThread(() => {
                    //ResetPhoneState();
                    controllerAction.ReleaseObject();
                }, 5f);
                break;
        }
    }

    void ShowRings() {
        iTween.ScaleTo(Hologram, iTween.Hash("scale", Vector3.one, "time", 1.0f, "delay", 0));
        foreach (Ring ring in rings)
        {
            ring.ShowRing();
        }
    }

    void HideRings()
    {
        iTween.ScaleTo(Hologram, iTween.Hash("scale", Vector3.zero, "time", 1.0f, "delay", 0));
        foreach (Ring ring in rings)
        {
            ring.HideRing();
        }
    }
    
    public void ResetState()
    {
        iTween.Stop(gameObject);
    }

    public void GoToStartPos()
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", startPos, "time", 2.0f, "delay", 0));
        iTween.RotateTo(gameObject, iTween.Hash("rotation", startRot, "time", 1.5f, "delay", 0));
        ResetPhoneState();
    }
    
    void ResetPhoneState()
    {
        SetSelectMode(0);
        SetSelectVideo(0);
        Products[VideoBtnIndex].HideObj();
    }

    public void RotateProduct(Vector2 rot)
    {
        Products[VideoBtnIndex].RotationObj(-rot.x, rot.y);
    }
}
