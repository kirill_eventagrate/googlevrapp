﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorOptions : MonoBehaviour
{
    public Texture TextureOptions;
    public GameObject Target;
    public StoreProduct Product;
    public Button btn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeTexture()
    {
        Product.UnSelectBtns();
        btn.interactable = false;
        Target.GetComponent<Renderer>().material.mainTexture = TextureOptions;
    }
}
