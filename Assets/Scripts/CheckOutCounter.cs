﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckOutCounter : MonoBehaviour
{
    static public CheckOutCounter instance = null;

    public GameObject[] Products;
    public Animator animator;

    //public GameObject EyeTarget;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChooseProduct(int _productIndex)
    {
        foreach (GameObject prod in Products)
        {
            prod.SetActive(false);
        }
        Products[_productIndex].SetActive(true);
    }
    public void StartAnimation()
    {
        animator.Play("CheckOutCounter", -1, 0);
        Loom.QueueOnMainThread(() => {
            PlayerScript.instance.GoToStartPos();
            animator.Play("CheckOutCounter", -1, 0);
            animator.StopPlayback();
        }, 9f);
    }
}
