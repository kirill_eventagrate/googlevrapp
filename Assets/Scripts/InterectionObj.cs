﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterectionObj : MonoBehaviour
{
    public float ScaleMin;
    public float ScaleMax;
    public float Sence;

    public ParticleSystem particleSys;
    //float scaleVar = 1;
    Quaternion originRotation;
    float angleX;
    float angleY;

    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        originRotation = transform.rotation;
        //animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RotationObj(float _senceY, float _senceX)
    {
        angleY += _senceY;
        //angleX += _senceX;
        Quaternion rotationY = Quaternion.AngleAxis(angleY * Sence, Vector3.up);
        //Quaternion rotationX = Quaternion.AngleAxis(angleX * Sence, Vector3.right);
        //scaleVar += _senceX;
        //scaleVar = Mathf.Clamp(scaleVar, ScaleMin, ScaleMax);
        //transform.localScale = new Vector3(scaleVar, scaleVar, scaleVar);
        transform.localRotation = originRotation * rotationY;
    }

    public void ResetRot()
    {
        angleY = 0;
        transform.localRotation = Quaternion.identity;
    }


    public void ShowObj()
    {
        /*animator.SetFloat("Direction", 1);
        animator.Play("ShowHideProduct", -1, 0);*/
        gameObject.SetActive(true);
        PlayerScript.instance.interectionObj = this;
        ResetRot();
        particleSys.Play();
    }

    public void HideObj()
    {
        /*animator.SetFloat("Direction", -1);
        animator.Play("ShowHideProduct", -1, 1);*/
        PlayerScript.instance.interectionObj = null;
        ResetRot();
        gameObject.SetActive(false);
    }
}
