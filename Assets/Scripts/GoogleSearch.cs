﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleSearch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        LapTopScript.instance.ShowGooglePreviewProd(true);
    }

    private void OnTriggerExit(Collider other)
    {
        LapTopScript.instance.ShowGooglePreviewProd(false);
        PlayerScript.instance.ChoosGoogle = false;
    }
}
