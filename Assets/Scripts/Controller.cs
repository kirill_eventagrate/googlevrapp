﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public InterectionObj TargetObj;

    //bool isObjShowing = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            TargetObj.RotationObj(-1, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            TargetObj.RotationObj(1, 0);
        }

        if (Input.GetKey(KeyCode.W))
        {
            TargetObj.RotationObj(0, 1);
        }

        if (Input.GetKey(KeyCode.S))
        {
            TargetObj.RotationObj(0, -1);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            /*if (!isObjShowing)
            {
                TargetObj.ShowObj();
                isObjShowing = true;
            }
            else
            {
                TargetObj.HideObj();
                isObjShowing = false;
            }*/
        }
    }
}
