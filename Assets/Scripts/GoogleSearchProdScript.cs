﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoogleSearchProdScript : MonoBehaviour
{
    
    public Button btn;
    public int SearchPageIndex;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ControllerAction>())
        {
            btn.interactable = false;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<ControllerAction>())
        {
            //btn.Select();
            btn.interactable = true;
        }
    }*/

    public void OnClick()
    {
        LapTopScript.instance.ShowSearchPage(SearchPageIndex);
        PlayerScript.instance.ChoosGoogle = true;
    }
}
