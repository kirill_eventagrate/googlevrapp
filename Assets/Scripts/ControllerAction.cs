﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;

public class ControllerAction : MonoBehaviour
{
    public SteamVR_Input_Sources handType; // 1
    public SteamVR_Action_Boolean teleportAction; // 2
    public SteamVR_Action_Boolean grabAction; // 3
    public SteamVR_Action_Boolean triggerAction; // 4

    public SteamVR_Action_Boolean UpTriggerAction; // 4
    public SteamVR_Action_Boolean RightTriggerAction; // 4
    public SteamVR_Action_Boolean DawnrTiggerAction; // 4
    public SteamVR_Action_Boolean LeftTriggerAction; // 4
    public SteamVR_Action_Vector2 touchPadAction;


    //public SteamVR_LaserPointer laserPointer;
    public SteamVR_Behaviour_Pose controllerPose;
    public GameObject Model;

    private GameObject collidingObject; // 1
    private GameObject objectInHand; // 2


    // Start is called before the first frame update
    void Start()
    {
        
    }
    

    private void SetCollidingObject(Collider col)
    {
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        collidingObject = col.gameObject;
        if (collidingObject.GetComponent<MainMenuButton>())
        {
            collidingObject.GetComponent<MainMenuButton>().toDoEvent.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (UpTriggerAction.GetStateDown(handType))
        {
            if (objectInHand && objectInHand.gameObject.GetComponent<PhoneScript>())
            {
                objectInHand.gameObject.GetComponent<PhoneScript>().DoUpAction();
            }

            if (collidingObject && collidingObject.gameObject.GetComponent<GoogleSearchPage>())
            {
                collidingObject.gameObject.GetComponent<GoogleSearchPage>().ResetSearchState();
            }
        }

        if (RightTriggerAction.GetStateDown(handType))
        {
            if (objectInHand && objectInHand.gameObject.GetComponent<PhoneScript>())
            {
                objectInHand.gameObject.GetComponent<PhoneScript>().DoRightAction();
            }
        }

        if (DawnrTiggerAction.GetStateDown(handType))
        {
            if (objectInHand && objectInHand.gameObject.GetComponent<PhoneScript>())
            {
                objectInHand.gameObject.GetComponent<PhoneScript>().DoDownAction();
            }
        }

        if (LeftTriggerAction.GetStateDown(handType))
        {
            if (objectInHand && objectInHand.gameObject.GetComponent<PhoneScript>())
            {
                objectInHand.gameObject.GetComponent<PhoneScript>().DoLeftAction();
            }
        }


        if (GetTeleportDown())
        {
            //print("Teleport " + handType);
        }

        if (GetGrabDown())
        {
            gameObject.GetComponent<SteamVR_LaserPointer>().Visible = true;
        }

        if (GetGrabUp())
        {
            gameObject.GetComponent<SteamVR_LaserPointer>().Visible = false;
        }

        /*if (GetGrabDown())
        {
            //gameObject.GetComponent<SteamVR_LaserPointer>().Visible = false;
            if (objectInHand && objectInHand.gameObject.GetComponent<PhoneScript>())
            {
                ReleaseObject();
            }else if (collidingObject)
            {
                if (collidingObject.gameObject.GetComponent<PhoneScript>())
                {
                    GrabObject();
                }
            }

            if (collidingObject && collidingObject.gameObject.GetComponent<StoreProduct>())
            {
                collidingObject.gameObject.GetComponent<StoreProduct>().DoAction();
            }
        }*/

        if (GetTriggerDown())
        {
            if (objectInHand && objectInHand.gameObject.GetComponent<PhoneScript>())
            {
                objectInHand.gameObject.GetComponent<PhoneScript>().DoAction();
            }

            /*if (collidingObject && collidingObject.gameObject.GetComponent<GoogleSearchProdScript>())
            {
                collidingObject.gameObject.GetComponent<GoogleSearchProdScript>().OnClick();
            }*/

            /*if (collidingObject && collidingObject.gameObject.GetComponent<GoogleSearchPage>())
            {
                collidingObject.gameObject.GetComponent<GoogleSearchPage>().OnClick();
            }*/
        }

        Vector2 touchPos = touchPadAction.GetAxis(handType);
        if (touchPos != Vector2.zero)
        {
            if (PlayerScript.instance.interectionObj)
            {
                PlayerScript.instance.interectionObj.RotationObj(-touchPos.x, touchPos.y);
                //objectInHand.gameObject.GetComponent<PhoneScript>().RotateProduct(touchPos);
            }
        }


        
    }
    
    public void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
    }
    
    public void OnTriggerStay(Collider other)
    {
        SetCollidingObject(other);
    }

    public void OnTriggerExit(Collider other)
    {
        if (!collidingObject)
        {
            return;
        }

        collidingObject = null;
    }

    private void GrabObject()
    {
        objectInHand = collidingObject;
        collidingObject = null;
        print("Grab obj: " + collidingObject);
        objectInHand.gameObject.GetComponent<PhoneScript>().ResetState();
        objectInHand.transform.SetParent(gameObject.transform);
        objectInHand.transform.localPosition = Vector3.zero;
        objectInHand.transform.localRotation = Quaternion.identity;
        Model.SetActive(false);
    }

    public void GrabObject(GameObject _GrabObj)
    {
        objectInHand = _GrabObj;
        collidingObject = null;
        print("Grab obj: " + collidingObject);
        if (objectInHand.gameObject.GetComponent<PhoneScript>()) {
            objectInHand.gameObject.GetComponent<PhoneScript>().controllerAction = this;
            objectInHand.gameObject.GetComponent<PhoneScript>().ResetState();
        }
        
        objectInHand.transform.SetParent(gameObject.transform);
        objectInHand.transform.localPosition = Vector3.zero;
        objectInHand.transform.localRotation = Quaternion.identity;
        Model.SetActive(false);
    }

    // 3
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    public void ReleaseObject()
    {
        objectInHand.transform.SetParent(null);
        if (objectInHand.gameObject.GetComponent<PhoneScript>())
        {
            objectInHand.gameObject.GetComponent<PhoneScript>().GoToStartPos();
            objectInHand.gameObject.GetComponent<PhoneScript>().controllerAction = null;
        }
        print("ReleaseObject");
        objectInHand = null;
        Model.SetActive(true);
    }

    public bool GetTriggerDown() // 1
    {
        return triggerAction.GetStateDown(handType);
    }

    public bool GetTeleportDown() // 1
    {
        return teleportAction.GetStateDown(handType);
    }

    public bool GetGrabDown() // 2
    {
        return grabAction.GetStateDown(handType);
    }

    public bool GetGrabUp() // 2
    {
        return grabAction.GetStateUp(handType);
    }
}
