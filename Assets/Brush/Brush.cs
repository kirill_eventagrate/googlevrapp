﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brush : MonoBehaviour
{
    public GameObject Parent;
    public Image theImage;
    public float animationTime = 1;
    // Start is called before the first frame update
    void Start()
    {
        //theImage = GetComponent<Image>();
        theImage.fillAmount = 0;
        
    }

    public void Spawn()
    {
        StartCoroutine(Fill());
        Destroy(Parent, 11f);
    }

    public void SetImage(Sprite _sprite)
    {
        theImage.sprite = _sprite;
    }

    IEnumerator Fill()
    {
        float elapsedTime = 0;
        float fillAmount = theImage.fillAmount;
        while (elapsedTime < animationTime)
        {
            theImage.fillAmount = Mathf.Lerp(fillAmount, 1, elapsedTime / animationTime);
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        theImage.fillAmount = 1;
    }
}
