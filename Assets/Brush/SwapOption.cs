﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapOption : MonoBehaviour
{
    public GameObject lightPrefab;
    public Sprite sprite;
    public StoreProduct Product;
    public Button btn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnSwap(GameObject _target)
    {
        Vector3 worldPos = _target.transform.position;
        GameObject go = Instantiate(lightPrefab);
        go.transform.position = worldPos;
        go.transform.GetChild(0).GetComponent<Brush>().SetImage(sprite);
        go.transform.GetChild(0).GetComponent<Brush>().Spawn();
        Product.UnSelectSwapBtns();
        btn.interactable = false;
    }
}
